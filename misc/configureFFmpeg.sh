#!/usr/bin/bash

./configure   \
--pkg-config-flags="--static" \
--disable-x86asm \
--disable-encoders \
--disable-swresample \
--disable-postproc \
--disable-muxers \
--disable-programs \
--disable-bsfs \
--disable-alsa \
--disable-outdevs \
--disable-bsfs \
--disable-doc \
--disable-shared \
--disable-network \
--disable-indevs \
\

exit

--disable-protocols \
--disable-demuxers \
--disable-parsers \
--disable-filters \

--disable-bzlib \
--disable-iconv \
--disable-libxcb \
--disable-libxcb-shape \
--disable-libxcb-shm \
--disable-libxcb-xfixes \
--disable-xlib \
--disable-zlib \
--disable-fft     \
--disable-faan    \
--disable-dct     \
--disable-dwt     \
--disable-lsp     \
--disable-lzo     \
--disable-mdct    \
--disable-rdft    \
--enable-gpl \
--enable-libx265 \
